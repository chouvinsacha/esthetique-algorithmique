
//                C H O U V I N      S A C H A     --    A L G O R I T H M    A E S T E T H I C

//                                      FIRST VERSION OF MY CODE

// -------------------
//  Parameters and UI
// -------------------

const gui = new dat.GUI()
const params = {
    N : 4,
    blackRdm : 0.6, //Jules suggested adding changing parameters.
    //I've chosen to vary the global rate of black squares in each box with an initial value of
    // 60%, as this was my first choice.
    Download_Image: () => save(),
}

gui.add(params, "blackRdm", 0, 1, 0.05)

// -------------------
//       Drawing
// -------------------
function draw()
{
    randomSeed(500)
    noStroke()
    imageMode(CENTER)
    background('white')

    const ecart = 17  //Space between each box
    const ccase = (height - 5*ecart)/params.N //length of each box
    const contour = 6 //Space between a box and the 5x5 grid in it
    
    for (let x1=width/2 - ccase - ecart/2 ; x1 < width/2 + ccase  ; x1 += ecart + ccase) //for 2 columns
    {  
        for (let y1=ecart ; y1 < height - ecart ; y1 += ecart + ccase) //for 5 lines
        {
            stroke('black')
            noFill()
            strokeWeight(3)
            rect(x1-contour, y1-contour, ccase+2*contour, ccase+2*contour)

            for (let x=x1 ; x < x1 + ccase ; x += ccase/5) 
            {
                for (let y=y1 ; y < y1 + ccase ; y += ccase/5)
                {
                    if (random() < params.blackRdm) //My first choice was a 60% rate of blacksquares
                    {              //Turns out there is 58% of black squares in the original artwork
                        fill('black')
                        strokeWeight(2)
                        stroke('black')
                        rect(x, y, ccase/5, ccase/5)
                    }

                    else
                    {
                        fill('white')
                        strokeWeight(2)
                        stroke('white')
                        rect(x, y, ccase/5, ccase/5)
                    }
                }  
            } 
        }
    }   
}

// -------------------
//    Initialization
// -------------------

function setup() {
    p6_CreateCanvas()
}

function windowResized() {
    p6_ResizeCanvas()
}