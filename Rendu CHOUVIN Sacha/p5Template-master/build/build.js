var gui = new dat.GUI();
var params = {
    N: 4,
    blackRdm: 0.6,
    Download_Image: function () { return save(); },
};
gui.add(params, "blackRdm", 0, 1, 0.05);
function draw() {
    randomSeed(500);
    noStroke();
    imageMode(CENTER);
    background('white');
    var ecart = 17;
    var ccase = (height - 5 * ecart) / params.N;
    var contour = 6;
    for (var x1 = width / 2 - ccase - ecart / 2; x1 < width / 2 + ccase; x1 += ecart + ccase) {
        for (var y1 = ecart; y1 < height - ecart; y1 += ecart + ccase) {
            stroke('black');
            noFill();
            strokeWeight(3);
            rect(x1 - contour, y1 - contour, ccase + 2 * contour, ccase + 2 * contour);
            for (var x = x1; x < x1 + ccase; x += ccase / 5) {
                for (var y = y1; y < y1 + ccase; y += ccase / 5) {
                    if (random() < params.blackRdm) {
                        fill('black');
                        strokeWeight(2);
                        stroke('black');
                        rect(x, y, ccase / 5, ccase / 5);
                    }
                    else {
                        fill('white');
                        strokeWeight(2);
                        stroke('white');
                        rect(x, y, ccase / 5, ccase / 5);
                    }
                }
            }
        }
    }
}
function setup() {
    p6_CreateCanvas();
}
function windowResized() {
    p6_ResizeCanvas();
}
var __ASPECT_RATIO = 1;
var __MARGIN_SIZE = 25;
function __desiredCanvasWidth() {
    var windowRatio = windowWidth / windowHeight;
    if (__ASPECT_RATIO > windowRatio) {
        return windowWidth - __MARGIN_SIZE * 2;
    }
    else {
        return __desiredCanvasHeight() * __ASPECT_RATIO;
    }
}
function __desiredCanvasHeight() {
    var windowRatio = windowWidth / windowHeight;
    if (__ASPECT_RATIO > windowRatio) {
        return __desiredCanvasWidth() / __ASPECT_RATIO;
    }
    else {
        return windowHeight - __MARGIN_SIZE * 2;
    }
}
var __canvas;
function __centerCanvas() {
    __canvas.position((windowWidth - width) / 2, (windowHeight - height) / 2);
}
function p6_CreateCanvas() {
    __canvas = createCanvas(__desiredCanvasWidth(), __desiredCanvasHeight());
    __centerCanvas();
}
function p6_ResizeCanvas() {
    resizeCanvas(__desiredCanvasWidth(), __desiredCanvasHeight());
    __centerCanvas();
}
var p6_SaveImageSequence = function (durationInFrames, fileExtension) {
    if (frameCount <= durationInFrames) {
        noLoop();
        var filename_1 = nf(frameCount - 1, ceil(log(durationInFrames) / log(10)));
        var mimeType = (function () {
            switch (fileExtension) {
                case 'png':
                    return 'image/png';
                case 'jpeg':
                case 'jpg':
                    return 'image/jpeg';
            }
        })();
        __canvas.elt.toBlob(function (blob) {
            p5.prototype.downloadFile(blob, filename_1, fileExtension);
            setTimeout(function () { return loop(); }, 100);
        }, mimeType);
    }
};
//# sourceMappingURL=../src/src/build.js.map