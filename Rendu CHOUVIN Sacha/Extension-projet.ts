
//                C H O U V I N      S A C H A     --    A L G O R I T H M    A E S T E T H I C

//                          EXTENSION OF MY CODE : A GROWING RATE OF BLACK SQUARES

// -------------------
//  Parameters and UI
// -------------------


const gui = new dat.GUI()
const params = {
    N : 4,
    Download_Image: () => save(),
}

// -------------------
//       Drawing
// -------------------

function draw()
{
    randomSeed(5)
    noStroke()
    imageMode(CENTER)
    background('white')

    const ecart = 17        //Space between each box 
    const ccase = (height-5*ecart)/params.N //length of each box
    const contour = 6       //Space between a box and the 5x5 grid in it
    let growingRdm = 0.4    //Starting value of growindRdm, for the first line

    /* NB : I had to invert the order of the first two "for" to paint in an order that follows the
    lines instead of the columns to keep the growindRdm the same */

    for (let y1=ecart ; y1 < height - ecart ; y1 += ecart + ccase) //for 5 lines
        {
            for (let x1=width/2 - ccase - ecart/2 ; x1 < width/2 + ccase  ; x1 += ecart + ccase) //for 2 columns
            {  
        
            stroke('black')
            noFill()
            strokeWeight(3)
            rect(x1-contour,y1-contour,ccase+2*contour,ccase+2*contour)//Creation of black boxes

            for (let x=x1 ; x < x1 + ccase ; x += ccase/5) 
            {
                for (let y=y1 ; y < y1 + ccase ; y +=ccase/5)
                {
                    if (random() < growingRdm){ //here is where i change a fixed rate to a "growing randomness" (growingRdm)
                       noStroke()
                        fill('black')
                        rect(x, y, ccase/5, ccase/5) //I paint the squares black with a probability of : 1 - growingRdm
                    }
                    else
                    {
                        noStroke()
                        fill('white')
                        rect(x, y, ccase/5, ccase/5) //I paint the others white
                    }
                        
                }  
            } 
        
        }
    growingRdm = growingRdm + 0.10 //The probability of painting a square black increases
                                   //every line (<=> every 2 boxes)
    }        
}

// -------------------
//    Initialization
// -------------------

function setup() {
    p6_CreateCanvas()
}

function windowResized() {
    p6_ResizeCanvas()
}


